import * as React from "react";
import PushDataTags from "@context/PushDataTag";

export default class Home extends React.Component<React.HTMLProps<HTMLElement>>{

    public render() {
        return (
            <>
                <PushDataTags>
                    {() =>
                        <>
                            <title>Website - Home Page</title>
                            <meta name="description" content="The home page of the website" />
                            <meta property="og:site_name" content="Website" />
                            <meta property="og:title" content="Home" />
                            <meta property="og:description" content="The home page of the website" />
                            <meta property="og:url" content="" />
                            <meta property="og:type" content="website" />
                        </>
                    }
                </PushDataTags>
                <div>
                    This is the home page of the website, it doesn't fetch any data
                </div>
            </>
        );
    }
}