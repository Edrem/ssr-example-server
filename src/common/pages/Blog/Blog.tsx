import * as React from "react";
import { PushDataStore } from "@context/PushDataContext";
import PushDataTags from "@context/PushDataTag";

interface IBlog {
    id: number;
    title: string
    content: string;
    post_date: string;
}

interface IIndexPageState {
    blog?: IBlog;
}

export default class Blog extends React.Component<React.HTMLProps<HTMLElement>, IIndexPageState>{
    constructor(props: React.HTMLProps<HTMLElement>) {
        super(props);

        this.state = { blog: undefined };
    }

    public componentDidMount() {
        PushDataStore.getResult("blog", 1)
            .then((data) => {
                this.setState({ blog: data.data.blog });
            });
    }

    public render() {
        return (
            <>
                <PushDataTags action="blog" args={[1]}>
                    {(data) =>
                        <>
                            <title>{data.data.blog.title} - Website</title>
                            <meta name="description" content={data.data.blog.content} />
                            <meta property="og:site_name" content="Website" />
                            <meta property="og:title" content={data.data.blog.title} />
                            <meta property="og:description" content={data.data.blog.content} />
                            <meta property="og:url" content="" />
                            <meta property="og:type" content="website" />
                        </>
                    }
                </PushDataTags>
                <section className="blog-area">
                    {this.state.blog &&
                        <article key={this.state.blog.id}>
                            <h1>{this.state.blog.title}</h1>
                            <p>{this.state.blog.content} </p>
                        </article>
                    }
                </section>
            </>
        );
    }
}