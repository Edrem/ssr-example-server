import * as React from "react";
import PushDataContext, { PushDataStore } from "@context/PushDataContext";
import PushDataTags from "@context/PushDataTag";
import { Link } from "react-router-dom";
import Blog from "../Blog/lazy";

interface IBlog {
    id: number;
    title: string
    content: string;
    post_date: string;
}

interface IIndexPageState {
    blogs: IBlog[];
}

export default class Blogs extends React.Component<React.HTMLProps<HTMLElement>, IIndexPageState>{
    constructor(props: React.HTMLProps<HTMLElement>) {
        super(props);

        this.state = { blogs: [] };
    }

    public async componentDidMount() {
        const data = await PushDataStore.getResult("blogs");
        this.setState({ blogs: data.data.blogs });
    }

    public render() {
        return (
            <>
                <PushDataContext.Consumer>
                    {(store) => {
                        store.runAction("blogs");
                        return null;
                    }}
                </PushDataContext.Consumer>
                <PushDataTags>
                    {() =>
                        <>
                            <title>Website - Blog Page</title>
                            <meta name="description" content="Blogposts on website" />
                            <meta property="og:site_name" content="Website" />
                            <meta property="og:title" content="Blogs" />
                            <meta property="og:description" content="Blogposts on website" />
                            <meta property="og:url" content="" />
                            <meta property="og:type" content="website" />
                        </>
                    }
                </PushDataTags>
                <section className="blog-area">
                    {this.state.blogs.map((item, index) => {
                        return (
                            <article key={item.id}>
                                <Link to={`/blog/${item.id}`} onMouseOver={Blog.preload} >{item.title}</Link>
                            </article>
                        );
                    })}
                </section>
            </>
        );
    }
}