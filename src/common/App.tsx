import * as React from "react";
import { NavLink, Route, Switch, Redirect } from "react-router-dom";
import Blogs from "./pages/Blogs/lazy";
import Blog from "./pages/Blog/lazy";
import style from "./App.scss";
import Home from "./pages/Home/lazy";

const Status: any = ({ code, extra, children }) => (
    <Route
        render={({ staticContext }) => {
            if (staticContext) {
                staticContext.code = code
                staticContext.extra = extra;
            };
            return children || "";
        }}
    />
);

export default class App extends React.Component<React.HTMLProps<HTMLElement>> {
    public render() {
        return (
            <Switch>
                <Route>
                    <div className={style["index-page"]}>
                        <header>
                            <h1>Website</h1>
                        </header>

                        <nav>
                            <NavLink to="/" onMouseOver={Home.preload} exact activeClassName={style.active}>Home</NavLink>
                            <NavLink to="/blogs" onMouseOver={Blogs.preload} exact activeClassName={style.active}>Blogs</NavLink>
                        </nav>
                        <main className={style.content}>
                            <Switch>
                                <Route path="/" exact component={Home} />
                                <Route path="/blogs/" exact component={Blogs} />
                                <Route path="/blog/:id" exact component={Blog} />
                                <Redirect from="//*" to="/*" />
                                <Status code={404} />
                            </Switch>
                        </main>
                    </div>
                </Route>
            </Switch>
        );
    }
}
