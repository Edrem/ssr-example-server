import * as React from "react";
import { btoa } from "abab";
import PushDataTags from "./PushDataTag";

const PushDataContext: React.Context<PushDataStore> = React.createContext();

type ActionFunction = (...args: any[]) => Promise<any>;

interface IAction {
    [index: string]: ActionFunction;
}

class PushDataStore {
    private static actions: IAction = {};
    private dataLoaders: { [index: string]: any } = {};
    private tagRenderer: PushDataTags;
    public static isServer = !global.window;
    private isFirstRun: boolean = true;

    public runAction(name: string, ...args: any[]) {
        if (!PushDataStore.isServer) {
            return;
        }
        const b64 = name + (btoa(JSON.stringify(args)) || "");
        if (!this.dataLoaders[b64]) {
            this.dataLoaders[b64] = {
                name,
                args,
            };
        }
    }

    // Server side version
    public async getResult(name: string, ...args: any[]) {
        return PushDataStore.actions[name](...args);
    }

    // Clientside version
    public static async getResult(name: string, ...args: any[]) {
        const b64 = name + (btoa(JSON.stringify(args)) || "");
        if (global.__pushData && global.__pushData[b64] !== undefined) {
            return global.__pushData[b64];
        }
        return PushDataStore.actions[name](...args);
    }

    public async getDataAsString() {
        const promises = Object.keys(this.dataLoaders).map((key) => {
            return PushDataStore.actions[this.dataLoaders[key].name](...this.dataLoaders[key].args);
        });
        const data = await Promise.all(promises);
        return `<script>
    window.__pushData = {}
        ${Object.keys(this.dataLoaders).map((key, index) => {
            return `window.__pushData['${key}'] = ${JSON.stringify(data[index]).replace(/</g, `\\u003c`)}`;
        })}
</script>`;
    }

    public async getTagsAsString() {
        if (this.tagRenderer) {
            return this.tagRenderer.renderTags();
        }
        return "";
    }

    public static registerAction(name: string, resolver: ActionFunction) {
        PushDataStore.actions[name] = resolver;
    }

    public registerTagRenderer(renderer: PushDataTags) {
        if (PushDataStore.isServer) {
            this.tagRenderer = renderer;
        }
    }

    public get firstRun() {
        const val = this.isFirstRun;
        this.isFirstRun = false;
        return val
    }
}

export default PushDataContext;
export { PushDataStore };