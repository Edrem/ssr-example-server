import * as React from "react";
import * as ReactDOM from "react-dom";

import { BrowserRouter, Route } from "react-router-dom";
import App from "@common/App";
import PushDataContext, { PushDataStore } from "@context/PushDataContext";

const blogs = () => {
    return fetch("/graphql", {
        method: "POST",
        headers: {
            "Content-Type": 'application/json',
            "Accept": 'application/json',
        },
        body: JSON.stringify({ query: "{ blogs {id, title, content }}" })
    })
        .then((r) => r.json())
};

const blog = (id: number) => {
    return fetch("/graphql", {
        method: "POST",
        headers: {
            "Content-Type": 'application/json',
            "Accept": 'application/json',
        },
        body: JSON.stringify({ query: `{ blog(id: ${id}) {id, title, content} }` })
    })
        .then((r) => r.json())
};

PushDataStore.registerAction("blogs", blogs);
PushDataStore.registerAction("blog", blog);

const pushManager = new PushDataStore();

ReactDOM.hydrate(
    <PushDataContext.Provider value={pushManager}>
        <BrowserRouter>
            <Route path="/" component={App} />
        </BrowserRouter>
    </PushDataContext.Provider>,
    document.getElementById("content")
);