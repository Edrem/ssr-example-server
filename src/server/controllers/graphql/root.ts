import { buildSchema } from "graphql";

const schema = buildSchema(`
    type BlogPost {
        id: Int!
        title: String!
        content: String!
    }

    type Query {
        blogs: [BlogPost!]!
        blog(id: Int!): BlogPost
    }

    type Mutation {
        addBlogPost(title: String!, content: String!): Boolean!
    }
`);

const root = {
    blogs: async () => {
        return [
            {
                id: 1,
                title: "Title",
                content: "Content",
            }
        ];
    },
    blog: async ({ id }) => {
        return {
            id: id,
            title: "Title",
            content: "Content",
        };
    },
    addBlogPost: async () => {
        // Do nothing
        return true;
    },
};

export { root, schema };
