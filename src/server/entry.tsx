import { renderToString } from "react-dom/server";
import { Route, StaticRouter } from "react-router";
import App from "@common/App";
import * as React from "react";
import flushChunks from 'webpack-flush-chunks';
import { clearChunks, flushChunkNames } from "react-universal-component/server";
import webpackStats from "../../build/stats.json";
import PushDataContext, { PushDataStore } from "@context/PushDataContext";

export default async function renderPage(context: any, url: string, devMode: boolean, req: Express.Request): Promise<[string, string[], string[]]> {
    const pushManager = new PushDataStore();
    // Clears previous chunks, this could be a big problem with async rendering
    clearChunks();
    const renderedString = renderToString(
        <PushDataContext.Provider value={pushManager}>
            <StaticRouter location={url} context={context}>
                <Route path="/" component={App} />
            </StaticRouter>
        </PushDataContext.Provider>
    );
    const pushData = await pushManager.getDataAsString();
    const tags = await pushManager.getTagsAsString();
    const { js, styles, css, scripts, stylesheets } = flushChunks(webpackStats, {
        chunkNames: flushChunkNames(),
        after: ["bundle"],
        outputPath: devMode ? "build/" : "public/",
    });

    return [`<!DOCTYPE html>
    <html lang="en" prefix="og: http://ogp.me/ns#">
    
    <head>
        ${tags}
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <link rel="icon" type="image/png" href="/images/favicon.png" />
        ${pushData}
        ${js}
        ${css}
    </head>
    
    <body>
        <div id="content">
            ${renderedString}
        </div>
    </body>
    
    </html>`,
        scripts,
        stylesheets,
    ];
}