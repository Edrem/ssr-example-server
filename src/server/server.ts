import * as express from "express";
import * as http from "http";
import * as path from "path";
import * as cors from "cors";
import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import renderPage from "./entry";
import * as graphQLHTTP from "express-graphql";
import * as compression from "compression";
import { schema, root } from "./controllers/graphql/root";
import { PushDataStore } from "@context/PushDataContext";

PushDataStore.registerAction("blogs", async () => {
    return {
        data: {
            blogs: await root.blogs(),
        },
    };
});

PushDataStore.registerAction("blog", async (id: number) => {
    return {
        data: {
            blog: await root.blog({ id }),
        },
    };
});

const app = express();
const devMode = process.argv.slice(2).indexOf("--dev") >= 0;

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json());
app.use(cookieParser());
const graphql = graphQLHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
});
app.use("/graphql", graphql);


if (!devMode) {
    app.get('*.js', function (req, res, next) {
        req.url = req.url + '.gz';
        res.set("Content-Type", "application/javascript");
        res.set('Content-Encoding', 'gzip');
        next();
    });
    app.get('*.css', function (req, res, next) {
        req.url = req.url + '.gz';
        res.set("Content-Type", "text/css");
        res.set('Content-Encoding', 'gzip');
        next();
    });
}

app.get('/:js.js', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    if (devMode) {
        res.sendFile(path.join(__dirname, `/build/${req.params.js}.js`));
    } else {
        res.sendFile(path.join(__dirname, `/public/${req.params.js}.js`));
    }
});

app.get('/:js.js.map', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    if (devMode) {
        res.sendFile(path.join(__dirname, `/build/${req.params.js}.js.map`));
    }
    else {
        res.sendFile(path.join(__dirname, `/public/${req.params.js}.js.map`));
    }
});

app.get('/:css.css.map', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    if (devMode) {
        res.sendFile(path.join(__dirname, `/build/${req.params.css}.css.map`));
    } else {
        res.sendFile(path.join(__dirname, `/public/${req.params.css}.css.map`));
    }
});

app.get('/:css.css', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    if (devMode) {
        res.sendFile(path.join(__dirname, `/build/${req.params.css}.css`));
    } else {
        res.sendFile(path.join(__dirname, `/public/${req.params.css}.css`));
    }
});

app.get('/:js.js.gz', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    if (devMode) {
        res.sendFile(path.join(__dirname, `/build/${req.params.js}.js.gz`));
    } else {
        res.sendFile(path.join(__dirname, `/public/${req.params.js}.js.gz`));
    }
});

app.get('/:css.css.gz', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    if (devMode) {
        res.sendFile(path.join(__dirname, `/build/${req.params.css}.css.gz`));
    } else {
        res.sendFile(path.join(__dirname, `/public/${req.params.css}.css.gz`));
    }
});

app.get('/favicon.ico', function (req, res) {
    res.set('Cache-Control', 'public, max-age=86400');
    res.sendFile(path.join(__dirname, `/public/images/favicon.png`));
});

app.use('/', express.static(path.join(__dirname, 'public'), {
    'extensions': ['html'],
}));

interface IContext {
    code?: number;
    url?: string;
    extra?: string;
}

/**
 *  Static routing
 */
app.use(compression());
app.get('*', async function (req, res) {
    const context: IContext = {};
    console.log("Requested URL", req.url);
    const t = await renderPage(context, req.url, devMode, req);
    if (context.code) {
        if (context.code >= 300 && context.code < 400) {
            res.redirect(context.code, context.extra);
        } else {
            res.status(context.code).send(context.extra || "");
        }
    } else {
        const preload = [...t[1].map((file) => `</${file}>; as=script; rel=preload`), ...t[2].map((file) => `</${file}>; as=style; rel=preload`)].join(",");
        res.set('Link', `${preload},</images/bg.png>; as=image; rel=preload`);
        res.send(t[0]);
    }
});

const server = http.createServer(app);
server.listen(8080, () => {
    console.log('HTTP server listening on port 8080');
});

export { server };
export default server;